import numpy, openravepy
import pylab as pl
from DiscreteEnvironment import DiscreteEnvironment
from copy import copy

class Control(object):
    def __init__(self, omega_left, omega_right, duration):
        self.ul = omega_left
        self.ur = omega_right
        self.dt = duration

class Action(object):
    def __init__(self, control, footprint):
        self.control = control
        self.footprint = footprint

class SimpleEnvironment(object):
    
    def __init__(self, herb, resolution):
        self.herb = herb
        self.robot = herb.robot

        self.boundary_limits = [[-3., -3., -numpy.pi], [3., 3., numpy.pi]]
        lower_limits,upper_limits = self.boundary_limits
        self.discrete_env = DiscreteEnvironment(resolution, lower_limits, upper_limits)
        self.env = self.robot.GetEnv()
        self.table = self.env.GetBodies()[1]
        self.resolution = resolution
        self.ConstructActions()

    def InitializePlot(self, goal_config):
        self.fig = pl.figure()
        pl.xlim([self.discrete_env.lower_limits[0], self.discrete_env.upper_limits[0]])
        pl.ylim([self.discrete_env.lower_limits[1], self.discrete_env.upper_limits[1]])
        pl.plot(goal_config[0], goal_config[1], 'gx')

        # Show all obstacles in environment
        for b in self.robot.GetEnv().GetBodies():
            if b.GetName() == self.robot.GetName():
                continue
            bb = b.ComputeAABB()
            pl.plot([bb.pos()[0] - bb.extents()[0],
                     bb.pos()[0] + bb.extents()[0],
                     bb.pos()[0] + bb.extents()[0],
                     bb.pos()[0] - bb.extents()[0],
                     bb.pos()[0] - bb.extents()[0]],
                    [bb.pos()[1] - bb.extents()[1],
                     bb.pos()[1] - bb.extents()[1],
                     bb.pos()[1] + bb.extents()[1],
                     bb.pos()[1] + bb.extents()[1],
                     bb.pos()[1] - bb.extents()[1]], 'r')
                    
                     
        pl.ion()
        pl.show()
        
    def PlotEdge(self, sconfig, econfig, color = 'k.-'):
        pl.plot([sconfig[0], econfig[0]],
                [sconfig[1], econfig[1]],
                color, linewidth=2.5)
        pl.draw()

    def plotActions(self, foobar):
        pass

    def GenerateFootprintFromControl(self, start_config, control, stepsize=0.01):

        # Extract the elements of the control
        ul = control.ul
        ur = control.ur
        dt = control.dt

        # Initialize the footprint
        config = start_config.copy()
        footprint = [numpy.array([0., 0., config[2]])]
        timecount = 0.0
        while timecount < dt:
            # Generate the velocities based on the forward model
            xdot = 0.5 * self.herb.wheel_radius * (ul + ur) * numpy.cos(config[2])
            ydot = 0.5 * self.herb.wheel_radius * (ul + ur) * numpy.sin(config[2])
            tdot = self.herb.wheel_radius * (ul - ur) / self.herb.wheel_distance
                
            # Feed forward the velocities
            if timecount + stepsize > dt:
                stepsize = dt - timecount
            config = config + stepsize*numpy.array([xdot, ydot, tdot])
            if config[2] > numpy.pi:
                config[2] -= 2.*numpy.pi
            if config[2] < -numpy.pi:
                config[2] += 2.*numpy.pi

            footprint_config = config.copy()
            footprint_config[:2] -= start_config[:2]
            footprint.append(footprint_config)

            timecount += stepsize
            
        # Add one more config that snaps the last point in the footprint to the center of the cell
        nid = self.discrete_env.ConfigurationToNodeId(config)
        snapped_config = self.discrete_env.NodeIdToConfiguration(nid)
        snapped_config[:2] -= start_config[:2]
        footprint.append(snapped_config)

        return footprint

    def PlotActionFootprints(self, idx):

        actions = self.actions[idx]
        fig = pl.figure()
        lower_limits, upper_limits = self.boundary_limits
        pl.xlim([lower_limits[0], upper_limits[0]])
        pl.ylim([lower_limits[1], upper_limits[1]])
        
        for action in actions:
            xpoints = [config[0] for config in action.footprint]
            ypoints = [config[1] for config in action.footprint]
            pl.plot(xpoints, ypoints, 'k')
                     
        pl.ion()
        pl.show()

    def ConstructControls(self):
        #Wl, Wr, Time

        #X,Y resolution is 0.1 
        drive_t = 0.5 #0.1257 #should be 0.1 meter for herb
        turn_t  = 0.25 #0.1563 #should be roughly 45 degrees (within discretization...)

        self.controls = []
        self.controls.append(Control(1, 1, drive_t)) #drive forward 1
        #self.controls.append(Control(1, 1, drive_t / 2.0)) #drive forward 1
        self.controls.append(Control(-1, -1, drive_t / 2.0)) #drive backwards 1/2
        
        #gradual turns
        self.controls.append(Control(0.7, 1, drive_t / 2.0)) #drive forward diagonal
        self.controls.append(Control(1, 0.7, drive_t / 2.0)) #drive backwards diagonal

        self.controls.append(Control(0.5, 1, drive_t / 2.0)) #drive forward diagonal
        self.controls.append(Control(1, 0.5, drive_t / 2.0)) #drive backwards diagonal
        
        #point turns
        self.controls.append(Control(1, -1, turn_t)) #turn right 45 
        self.controls.append(Control(-1, 1, turn_t)) #turn left 45

    def ConstructActions(self):
        # Actions is a dictionary that maps orientation of the robot to  an action set
        self.actions = dict()
              
        wc = [0., 0., 0.]
        grid_coordinate = self.discrete_env.ConfigurationToGridCoord(wc)

        self.ConstructControls()

        # Iterate through each possible starting orientation
        for idx in range(int(self.discrete_env.num_cells[2])):
            self.actions[idx] = []
            grid_coordinate[2] = idx
            start_config = self.discrete_env.GridCoordToConfiguration(grid_coordinate)

            for control in self.controls:
                footprint = self.GenerateFootprintFromControl(start_config,control)
                self.actions[idx].append(Action(control, footprint))

    def GetSuccessors(self, node_id):
        successors = []
        curr_config  = self.discrete_env.NodeIdToConfiguration(node_id)
        curr_grid    = self.discrete_env.NodeIdToGridCoord(node_id)
        
        for action in self.actions[curr_grid[2]]:
            # TODO: Might have to check for all configs in footprint provided foot print is large
            last_footprint = action.footprint[-1] #Get the ending configuration
            offset_config  = numpy.zeros(len(last_footprint))

            offset_config[0:2] = curr_config[0:2] + last_footprint[0:2]
            offset_config[2]   = last_footprint[2]
            
            if self.is_valid_move(offset_config):
                successor_node_id = self.discrete_env.ConfigurationToNodeId(offset_config)
                successors.append((successor_node_id, action))

        return successors

    def is_valid_move(self,config):
        #check bounds
        lower_limits, upper_limits = self.boundary_limits
        invalid = ((config < lower_limits).any() or (config > upper_limits).any())
        
        #check collisions
        if not invalid:
            init_pose = self.robot.GetTransform() #Copy current transfom to reset robot
            #Calculate pose from new position vector, new orientation 
            new_pose = openravepy.matrixFromAxisAngle([0,0, config[2]])
            new_pose[0][3] = config[0]
            new_pose[1][3] = config[1]

            with self.env: #Lock Environment before changing robot pose
                self.robot.SetTransform(new_pose)
                invalid = (self.env.CheckCollision(self.robot, self.table))
                self.robot.SetTransform(init_pose)

        return not invalid

    def ComputeDistance(self, start_id, end_id):
        start_config = self.discrete_env.NodeIdToConfiguration(start_id)
        end_config   = self.discrete_env.NodeIdToConfiguration(end_id)
        dist = numpy.linalg.norm(start_config[0:2] - end_config[0:2])
        return dist

    def ComputeHeuristicCost(self, start_id, end_id):
        start_config = self.discrete_env.NodeIdToConfiguration(start_id)
        end_config   = self.discrete_env.NodeIdToConfiguration(end_id)
        #TODO: weight orientation much lower?
        return numpy.linalg.norm(start_config[0:2] - end_config[0:2]) #numpy.linalg.norm(start_config - end_config)
