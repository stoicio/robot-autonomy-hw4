import numpy
from DiscreteEnvironment import DiscreteEnvironment
from openravepy import databases, IkParameterization
import random
import time

MAX_DISTANCE = 0.75
import IPython


class HerbEnvironment(object):
    
    def __init__(self, herb, resolution):
        self.herb = herb # added by kdr
        self.robot = herb.robot
        self.robot_name = self.robot.GetName()
        
        self.lower_limits, self.upper_limits = self.robot.GetActiveDOFLimits()
        self.discrete_env = DiscreteEnvironment([resolution] * 7, self.lower_limits, self.upper_limits)
        self.resolution = resolution
        self.robot_name = self.robot.GetName()

        self.env = self.robot.GetEnv()
        # account for the fact that snapping to the middle of the grid cell may put us over our
        #  upper limit
        upper_coord = [x - 1 for x in self.discrete_env.num_cells]
        upper_config = self.discrete_env.GridCoordToConfiguration(upper_coord)
        for idx in range(len(upper_config)):
            self.discrete_env.num_cells[idx] -= 1

        self.ik = databases.inversekinematics.InverseKinematicsModel(self.robot,iktype=IkParameterization.Type.Translation3D)
        #Get Joint Weights 
        self.joint_weights = self.robot.GetActiveDOFWeights()
        self.init_pos = self.robot.GetActiveDOFValues()
    
    def isColliding(self,config):
        init_pos = self.robot.GetActiveDOFValues() #Copy current transfom to reset robot
           
        with self.env: #Lock Environment before changing robot pose
            self.robot.SetActiveDOFValues(config)
            is_colliding1 = self.env.CheckCollision(self.env.GetBodies()[0],self.env.GetBodies()[1])
            is_colliding2 = self.robot.CheckSelfCollision()
            is_colliding = is_colliding1 or is_colliding2

        self.robot.SetActiveDOFValues(init_pos) 
        return is_colliding

    def GetSuccessors(self, node_id):
        successors = []
        denv = self.discrete_env
        config = self.discrete_env.NodeIdToConfiguration(node_id)
        coord = self.discrete_env.NodeIdToGridCoord(node_id)
        successors = [None]*2*len(self.lower_limits)
        count = 0

        for i in range(denv.dimension):
            nodesPerDim[i] = (self.upper_limits[i] - self.lower_limits[i])/self.resolution

            if (coord[i]>0):
                newConfig = config[::]
                newConfig[i] = config[i] - self.resolution
                if not self.isColliding(newConfig):
                    successors[count] = self.discrete_env.ConfigurationToNodeId(newConfig)
                else:
                    successors[count] = None

            if (coord[i]<nodesPerDim[i]-1):
                newConfig = config[::]
                newConfig[i] = config[i] + self.resolution
                if not self.isColliding(newConfig):
                    successors[count+1] = self.discrete_env.ConfigurationToNodeId(newConfig)
                else:
                    successors[count+1] = None
            count += 2
        successors = [x for x in successors if x != None]
        return successors

    # added by kdr
    def ComputeDistanceConfig(self, start_config, end_config):
        start_id = self.discrete_env.ConfigurationToNodeId(start_config)
        end_id = self.discrete_env.ConfigurationToNodeId(end_config)
        return self.ComputeDistance(start_id,end_id)


    def ComputeDistance(self, start_id, end_id):
        dist = 0
        # TODO: Here you will implement a function that 
        # computes the distance between the configurations given
        # by the two node ids
        if (type(start_id) is not int):
            config_start = start_id
            config_end   = end_id
        else:
            config_start = self.discrete_env.NodeIdToConfiguration(start_id)
            config_end = self.discrete_env.NodeIdToConfiguration(end_id)

        with self.env:
            self.robot.SetActiveDOFValues(config_start)
        start_pos = self.ik.manip.GetTransform()[0:3,3] #Get 3D Position of End Effector
        
        with self.env:
            self.robot.SetActiveDOFValues(config_end)
        end_pos = self.ik.manip.GetTransform()[0:3,3] #Get 3D Position of End Effector
        
        with self.env:
            self.robot.SetActiveDOFValues(self.init_pos)
        dist = numpy.linalg.norm(start_pos - end_pos)
       
        return dist

    def ComputeHeuristicCost(self, start_id, goal_id):
        
        cost = 0

        # TODO: Here you will implement a function that 
        # computes the heuristic cost between the configurations
        # given by the two node ids
        if (type(start_id) is not int):
            cost = self.ComputeDistance(start_id, goal_id)
        else:
            config_start = self.discrete_env.NodeIdToConfiguration(start_id)
            config_end = self.discrete_env.NodeIdToConfiguration(goal_id)

            for i in range(0,len(config_start)):
                cost = cost + abs(config_start[i] - config_end[i])

        return cost

    def SetGoalParameters(self, goal_config, p = 0.2):
        self.goal_config = goal_config
        self.p = p
        

    def GenerateRandomConfiguration(self):
        rand_p = numpy.random.ranf()
        if (0 < rand_p < self.p):
            return self.goal_config
        elif (self.p < rand_p < 1):
            config = [0] * len(self.robot.GetActiveDOFIndices())
            lower_limits, upper_limits = self.robot.GetActiveDOFLimits()

        #Generate Raom Numbers
        for i in range(0,len(config)):
            config[i] = random.uniform(lower_limits[i],upper_limits[i])

        #Check for Collision
        with self.env: #Lock Environment before changing robot pose
            self.robot.SetActiveDOFValues(config)
            is_colliding1 = self.env.CheckCollision(self.env.GetBodies()[0],self.env.GetBodies()[1])
            is_colliding2 = self.robot.CheckSelfCollision()
            is_colliding = is_colliding1 or is_colliding2

        #Generate new positions until it is collision free
        while is_colliding:
            for i in range(0,len(config)):
                config[i] = random.uniform(lower_limits[i],upper_limits[i])

            with self.env: #Lock Environment before changing robot pose
                self.robot.SetActiveDOFValues(config)
                is_colliding1 = self.env.CheckCollision(self.env.GetBodies()[0],self.env.GetBodies()[1])
                is_colliding2 = self.robot.CheckSelfCollision()
                is_colliding = is_colliding1 or is_colliding2
                 
        with self.env: #Set Robot back to initial pose
            self.robot.SetActiveDOFValues(self.init_pos)       
        return numpy.array(config)

    
    def Extend(self, start_config, end_config,limit_d = True):
        
        sample = 100
        config_interp = numpy.zeros((sample,7))

        #Interpolation of X and Y values
        for i in xrange(0,len(start_config)):
            config_interp[:,i] = numpy.linspace(start_config[i], end_config[i], num=sample)

        for i in xrange(0,sample):
            temp_pos = config_interp[i][:]

            #Check for collision
            with self.env:
                self.robot.SetActiveDOFValues(temp_pos)
                is_colliding1 = self.env.CheckCollision(self.env.GetBodies()[0],self.env.GetBodies()[1])
                is_colliding2 = self.robot.CheckSelfCollision()
                is_colliding = is_colliding1 or is_colliding2

            if is_colliding:
                return True,None

            if limit_d == True:
                curr_distance = self.ComputeDistance(start_config,temp_pos)
                if (curr_distance >= MAX_DISTANCE):
                    return False,numpy.array(temp_pos)
        return False,end_config

    def ShortenPath(self, path, timeout=5.0):
        to_exit = time.time()+timeout
        while (time.time() < to_exit):
            vertex_1, vertex_2 = sorted(random.sample(range(len(path)),2))
            if vertex_1+1 == vertex_2:
                continue
            is_colliding = self.Extend(path[vertex_1],path[vertex_2],limit_d = False)[0]

            if is_colliding == False:
                for i in range(vertex_1+1,vertex_2):
                    path[i] = None
            path = [x for x in path if x != None]
        return path
