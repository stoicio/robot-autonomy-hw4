import time
class Planner(object):
    def __init__(self, planning_env, visualize):
        self.planning_env = planning_env
        self.visualize = visualize and hasattr(self.planning_env, 'InitializePlot')
        self.path_length = 0.0
        self.node_count = 0
        self.plan_time = 0
        
    def Plan(self, start_config, goal_config, epsilon = 0.001):
        """planner wrapper function to abstract statistics away from individual planners"""
        if self.visualize:
            self.planning_env.InitializePlot(goal_config)

        start_time = time.clock()
        plan = self.DoPlan(start_config, goal_config, epsilon)
        self.plan_time = time.clock() - start_time

        if plan: #non-empty path
            return plan
        else:
            return None #obvious planning failure

    def DoPlan(self, start_config, goal_config, epsilon):
        return None

from AStarPlanner import AStarPlanner
from HeuristicRRTPlanner import HeuristicRRTPlanner