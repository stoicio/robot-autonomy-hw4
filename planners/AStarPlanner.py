import heapq
import IPython
from . import Planner

class AStarPlanner(Planner): 
    def DoPlan(self, start_config, goal_config, epsilon):

        goalNode = self.planning_env.discrete_env.ConfigurationToNode(goal_config)
        startNode = self.planning_env.discrete_env.ConfigurationToNode(start_config)
        startNode.gCost = 0.0
        startNode.setHeuristicCost(self.planning_env.ComputeHeuristicCost(startNode.node_id, goalNode.node_id))
        
        closedList = []
        openList = [startNode]

        while len(openList):
            currNode = heapq.heappop(openList)
            successors = self.planning_env.GetSuccessors(currNode.node_id)
            #print 'successors: ' + str(successors)
            for node_id, action in successors:
                #update next node
                nextNode = self.planning_env.discrete_env.GetNode(node_id)

                gCost = currNode.gCost + self.planning_env.ComputeDistance(currNode.node_id, node_id)
                nextNode.setParent(gCost, currNode, action)
                nextNode.setHeuristicCost(2.5 * self.planning_env.ComputeHeuristicCost(node_id, goalNode.node_id))
                
                if not (nextNode in closedList):
                    heapq.heappush(openList, nextNode)
                #else:
                #    print 'closed'

                if nextNode == goalNode:
                    self.path_length = nextNode.gCost
                    self.node_count = len(closedList)
                    return nextNode.getPathToStart()

            closedList.append(currNode)
            #print 'openList: ' + str(len(openList)) + " : " + str(currNode.hCost)
            if self.visualize:
                self.planning_env.PlotEdge(currNode.config, nextNode.config)

        self.node_count = len(closedList)
        return None
