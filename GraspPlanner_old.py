import logging, numpy, openravepy
np = numpy

class GraspPlanner(object):

    def __init__(self, robot, base_planner, arm_planner):
        self.robot = robot
        self.base_planner = base_planner
        self.arm_planner = arm_planner
        # import IPython
        # IPython.embed()
        # self.target_kinbody = object

            
    def GetBasePoseForObjectGrasp(self, obj):

        # Load grasp database
        self.gmodel = openravepy.databases.grasping.GraspingModel(self.robot, obj)
        if not self.gmodel.load():
            gmodel.autogenerate()

        base_pose = None
        grasp_config = None

        ###################################################################
        # TODO: Here you will fill in the function to compute
        #  a base pose and associated grasp config for the 
        #  grasping the bottle
        ###################################################################
        
        self.graspindices = self.gmodel.graspindices
        self.grasps = self.gmodel.grasps
        
        self.order_grasps();

        # set grasp config to best grasp
        grasp_config = self.grasps_ordered[0]

        import IPython
        IPython.embed()
        # inverse reachability stuff
        
        return base_pose, grasp_config

    # function to evaluate grasps
    # returns a score, which is some metric of the grasp
    # higher score should be a better grasp
    def eval_grasp(self, grasp):
      with self.robot:
        #contacts is a 2d array, where contacts[i,0-2] are the positions of contact i and contacts[i,3-5] is the direction
        try:
          contacts,finalconfig,mindist,volume = self.gmodel.testGrasp(grasp=grasp,translate=True,forceclosure=False)

      # If no contacts - score is zero
          if (len(contacts) == 0):
            return 0

          obj_position = self.gmodel.target.GetTransform()[0:3,3]
          # for each contact
          count=0;
          G = np.zeros((6,len(contacts))) #the wrench matrix
          for c in contacts:
            pos = c[0:3] - obj_position
            dirtn = -c[3:] #this is already a unit vector
            #TODO fill G
            temp = np.cross(pos,dirtn)
            G[0][count] = dirtn[0]
            G[1][count] = dirtn[1]
            G[2][count] = dirtn[2]
            G[3][count] = temp[0]
            G[4][count] = temp[1]
            G[5][count] = temp[2]
            count=count+1 

          #TODO use G to compute scrores as discussed in class
          U, s, V = np.linalg.svd(G)
          sig_min = min(s)
          sig_max = max(s)
          #m0 = self.gmodel.EvaluateGrasp(grasp)
          #print(m0)
          #m0 = -self.gmodel._ComputeGraspPerformance(grasp)
          ab = self.target_kinbody.ComputeAABB()
          m0 = np.min(sum((contacts[:,0:3]-np.tile(ab.pos(),(len(contacts),1)))**2,1))-1
          #print m0
          m1 = sig_min
          m2 = sig_min/sig_max
          m3 = np.sqrt(np.linalg.det(np.dot(G,G.T)))
          if math.isnan(m3) == True:
            return 0
          w0 = 1;
          w1 = 0.33; #0.33 #1
          w2 = 1.66; #1.66 #5
          w3 = 3.33; #3.33 #10 
          #print 'val:'
          #print w0*m0
          #print w1*m1
          #print w2*m2
          #print w3*m3
          val = (w0*m0)+(w1*m1)+(w2*m2)+(w3*m3)
          #print val
          return  val

        except openravepy.planning_error,e:
          #you get here if there is a failure in planning
          #example: if the hand is already intersecting the object at the initial position/orientation
          return  0.00 # TODO you may want to change this
    
    # Output first N grasps
    def show_first_n(self,grasps,order,n):
      # show the 5 highest ranked grasps
      for i in range(0, n):
        grasp = grasps[i]
        print "Grasp:", order[i]
        print "Performance:", grasp[self.graspindices.get('performance')]
        with self.env:
      #self.show_grasp(grasp)
          contacts,finalconfig,mindist,volume = self.gmodel.testGrasp(grasp=grasp,translate=True,forceclosure=True,graspingnoise=None)
          contactgraph = self.gmodel.drawContacts(contacts) if len(contacts) > 0 else None
          self.robot.GetController().Reset(0)
          self.robot.SetDOFValues(finalconfig[0])
          self.robot.SetTransform(finalconfig[1])
          self.env.UpdatePublishedBodies()
      #time.sleep(1)
          raw_input('press any key to continue: ')

    # order the grasps - call eval grasp on each, set the 'performance' index, and sort
    def order_grasps(self):
      print "Ordering grasps..."
      self.grasps_ordered = self.grasps.copy() #you should change the order of self.grasps_ordered
      for grasp in self.grasps_ordered:
        grasp[self.graspindices.get('performance')] = self.eval_grasp(grasp)
      
      # sort!
      order = np.argsort(self.grasps_ordered[:,self.graspindices.get('performance')[0]])
      order = order[::-1]
      self.grasps_ordered = self.grasps_ordered[order]
      
      # show 4 highest ranked grasps
      self.show_first_n(self.grasps_ordered,order,4)

    def PlanToGrasp(self, obj):

        # Next select a pose for the base and an associated ik for the arm
        base_pose, grasp_config = self.GetBasePoseForObjectGrasp(obj)

        if base_pose is None or grasp_config is None:
            print 'Failed to find solution'
            exit()

        # Now plan to the base pose
        start_pose = numpy.array(self.base_planner.planning_env.herb.GetCurrentConfiguration())
        base_plan = self.base_planner.Plan(start_pose, base_pose)
        base_traj = self.base_planner.planning_env.herb.ConvertPlanToTrajectory(base_plan)

        print 'Executing base trajectory'
        self.base_planner.planning_env.herb.ExecuteTrajectory(base_traj)



        # Now plan the arm to the grasp configuration
        start_config = numpy.array(self.arm_planner.planning_env.herb.GetCurrentConfiguration())
        arm_plan = self.arm_planner.Plan(start_config, grasp_config)
        arm_traj = self.arm_planner.planning_env.herb.ConvertPlanToTrajectory(arm_plan)

        print 'Executing arm trajectory'
        self.arm_planner.planning_env.herb.ExecuteTrajectory(arm_traj)

        # Grasp the bottle
        task_manipulation = openravepy.interfaces.TaskManipulation(self.robot)
        task_manipultion.CloseFingers()
    

