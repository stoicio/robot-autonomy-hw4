import logging, numpy, openravepy, math, time, IPython
np = numpy 
class GraspPlanner(object):

    def __init__(self, robot, base_planner, arm_planner):
        self.robot = robot
        self.base_planner = base_planner
        self.arm_planner = arm_planner
        self.manip = self.robot.GetActiveManipulator()


            
    def GetBasePoseForObjectGrasp(self, obj):

        # Load grasp database
        self.gmodel = openravepy.databases.grasping.GraspingModel(self.robot, obj)
        if not self.gmodel.load():
            self.gmodel.autogenerate()

        start_pose = numpy.array(self.base_planner.planning_env.herb.GetCurrentConfiguration())
        #print start_pose
        #self.base_planner.planning_env.InitializePlot(start_pose)

        base_pose = None
        grasp_config = None
       
        ###################################################################
        # TODO: Here you will fill in the function to compute
        #  a base pose and associated grasp config for the 
        #  grasping the bottle
        ###################################################################
        
        self.graspindices = self.gmodel.graspindices
        self.grasps = self.gmodel.grasps

        self.order_grasps();

        # set grasp config to best grasp
        grasp_config = self.grasps_ordered[0]
        Tgrasp = self.gmodel.getGlobalGraspTransform(grasp_config, collisionfree=True)

        # inverse reachability stuff follows

        # load inverserechability database
        self.irmodel = openravepy.databases.inversereachability.InverseReachabilityModel(robot=self.robot)
        starttime = time.time()
        print 'loading irmodel'
        if not self.irmodel.load():
            print 'do you want to generate irmodel for your robot? it might take several hours'
            print 'or you can go to http://people.csail.mit.edu/liuhuan/pr2/openrave/openrave_database/ to get the database for PR2'
            input = raw_input('[Y/n]')
            if input == 'y' or input == 'Y' or input == '\n' or input == '':
                class IrmodelOption:
                    self.irmodel.autogenerate()
                    self.irmodel.load()
            else:
                raise ValueError('')
                
        print 'time to load inverse-reachability model: %fs'%(time.time()-starttime)


        densityfn,samplerfn,bounds = self.irmodel.computeBaseDistribution(Tgrasp)

        N = 10
        # find valid poses
        goals = []
        numfailures = 0
        starttime = time.time()
        timeout = float('Inf')
        with self.robot:
            while len(goals) < N:
                if time.time()-starttime > timeout:
                    break
                poses,jointstate = samplerfn(N-len(goals))
                for pose in poses:

                    # # snap to grid
                    # self.robot.SetTransform(pose)
                    # pose3 = numpy.array(self.base_planner.planning_env.herb.GetCurrentConfiguration())
                    
                    # pose_id = self.base_planner.planning_env.discrete_env.ConfigurationToNodeId(pose3)
                    # pose3_new = self.base_planner.planning_env.discrete_env.NodeIdToConfiguration(pose_id)
                    # self.base_planner.planning_env.herb.SetCurrentConfiguration(pose3_new)
                    # pose = self.robot.GetTransform()

                    # IPython.embed()

                    self.robot.SetTransform(pose)
                    self.robot.SetDOFValues(*jointstate)
                    # validate that base is not in collision
                    if not self.checkCollision():
                        q = self.manip.FindIKSolution(Tgrasp,filteroptions=openravepy.IkFilterOptions.CheckEnvCollisions)
                        if q is not None:
                            values = self.robot.GetDOFValues()
                            values[self.manip.GetArmIndices()] = q
                            #config = numpy.array(self.base_planner.planning_env.herb.GetCurrentConfiguration())
                            goals.append((Tgrasp,pose,values))
                        elif self.manip.FindIKSolution(Tgrasp,0) is None:
                            numfailures += 1

        dist = [0]*N
        for i in range(N):
            config = self.poseToConfig(goals[i][1])
            #print config
            #self.base_planner.planning_env.PlotEdge(config, config)
            dist[i] = numpy.linalg.norm(config[0:2] - start_pose[0:2]) 
        i = dist.index(min(dist))

        base_pose = goals[i][1]
        grasp_config = goals[i][2][self.manip.GetArmIndices()]

        # self.checkFinalGrasp(base_pose,grasp_config)

        return base_pose, grasp_config

    def checkCollision(self):
        result = False
        result = result or self.robot.CheckSelfCollision()

        env = self.robot.GetEnv()
        bodies = env.GetBodies()

        # print('debug checkCollision')
        # import IPython
        # IPython.embed()

        for bIter in xrange(len(bodies)-1):
            result = result or env.CheckCollision(self.robot,bodies[bIter+1])
            if result == True:
                break

        return result


    def checkFinalGrasp(self, base_pose, grasp_config):
        # to be finished

        t = self.robot.GetTransform()

        self.robot.SetTransform(base_pose)
        self.robot.SetActiveDOFValues(grasp_config)
        
        raw_input('enter something')

        self.robot.SetTransform(t)
        
        # base_3pose = numpy.array(self.base_planner.planning_env.herb.GetCurrentConfiguration())
        # self.base_planner.planning_env.herb.SetCurrentConfiguration(base_3pose)

        # IPython.embed()


    def show_grasp(self, grasp, delay=1.5):
        with openravepy.RobotStateSaver(self.gmodel.robot):
          with self.gmodel.GripperVisibility(self.gmodel.manip):
            time.sleep(0.1) # let viewer update?
            try:
              env = self.robot.GetEnv()
              with env:
                contacts,finalconfig,mindist,volume = self.gmodel.testGrasp(grasp=grasp,translate=True,forceclosure=True)
                #if mindist == 0:
                #  print 'grasp is not in force closure!'
                contactgraph = self.gmodel.drawContacts(contacts) if len(contacts) > 0 else None
                self.gmodel.robot.GetController().Reset(0)
                self.gmodel.robot.SetDOFValues(finalconfig[0])
                self.gmodel.robot.SetTransform(finalconfig[1])
                env.UpdatePublishedBodies()
                time.sleep(delay)
            except openravepy.planning_error,e:
              print 'bad grasp!',e

    # order the grasps - call eval grasp on each, set the 'performance' index, and sort
    def order_grasps(self):
        self.grasps_ordered = self.grasps.copy() #you should change the order of self.grasps_ordered
        gs = []
        giso = []
        gvol = []
        for grasp in self.grasps_ordered:
          g = self.eval_grasp(grasp)
          gs.append(g[0])
          giso.append(g[1])
          gvol.append(g[2])
        gs_n = []
        giso_n = []
        gvol_n = []
        for i in range(len(gs)):
          gs_n.append((gs[i]-min(gs))/(max(gs)-min(gs)))
          giso_n.append((giso[i]-min(giso))/(max(giso)-min(giso)))
          gvol_n.append((gvol[i]-min(gvol))/(max(gvol)-min(gvol)))
          grasp[self.graspindices.get('performance')] = gs_n[i] + giso_n[i] + gvol_n[i] #linear combination of normalized metrics
          

        # sort!
        order = np.argsort(self.grasps_ordered[:,self.graspindices.get('performance')[0]])
        order = order[::-1]
        self.grasps_ordered = self.grasps_ordered[order]
        #size = len(self.grasps_ordered)
        #for i in range(size):
        #raw_input('press key to continue :')
            #print (self.grasps_ordered[i][12])
        #self.show_grasp(self.grasps_ordered[i])

    def poseToConfig(self, pose):
        with self.robot:
            self.robot.SetTransform(pose)
            config = numpy.array(self.base_planner.planning_env.herb.GetCurrentConfiguration())
        return config
 
    # function to evaluate grasps
    # returns a score, which is some metric of the grasp
    # higher score should be a better grasp
    def eval_grasp(self, grasp):
      with self.robot:
        #contacts is a 2d array, where contacts[i,0-2] are the positions of contact i and contacts[i,3-5] is the direction
        try:
          contacts,finalconfig,mindist,volume = self.gmodel.testGrasp(grasp=grasp,translate=True,forceclosure=False)
          obj_position = self.gmodel.target.GetTransform()[0:3,3]
          # for each contact
          G = np.array([]) #the wrench matrix
          g = []
          W = np.array([])
          count = 0
          for c in contacts:
            pos = c[0:3] - obj_position
            dir = -c[3:] #this is already a unit vector
            d = np.cross(pos, dir)
            W = np.vstack((pos, d))
            W = np.reshape(W, (6,1))
            if count == 0:
              G = W
            else:
              G = np.hstack((G,W))      
            count = count+1

          #TODO fill G
          [U,S,V] = np.linalg.svd(G)
          smin = min(S)
          smax = max(S)
          iso = smin/smax
          
          # import IPython
          # IPython.embed()

          vol = np.sqrt(np.linalg.det(np.dot(G,(np.transpose(G)))))
          if math.isnan(vol):
             return (smin,iso,0)
          else:
             #TODO use G to compute scrores as discussed in class
             return (smin,iso,vol) #change this
        except openravepy.planning_error,e:
          #you get here if there is a failure in planning
          #example: if the hand is already intersecting the object at the initial position/orientation
          return  (0,0,0) # TODO you may want to change this
 

    def PlanToGrasp(self, obj):

        # Next select a pose for the base and an associated ik for the arm
        base_pose, grasp_config = self.GetBasePoseForObjectGrasp(obj)

        if base_pose is None or grasp_config is None:
            print 'Failed to find solution'
            exit()

        base_config = self.poseToConfig(base_pose)

        # Now plan to the base pose
        print 'running A*'
        start_pose = numpy.array(self.base_planner.planning_env.herb.GetCurrentConfiguration())
        base_plan = self.base_planner.Plan(start_pose, base_config)
        if base_plan == None:
            print 'No base plan found!'
            exit()

        #base_pose)
        base_traj = self.base_planner.planning_env.herb.ConvertPlanToTrajectory(base_plan)

        self.base_planner.planning_env.herb.ExecuteTrajectory(base_traj)

        #snap to correct final point
        self.robot.SetTransform(base_pose)
        print 'Planning arm trajectory.'

        # Now plan the arm to the grasp configuration
        start_config = numpy.array(self.arm_planner.planning_env.herb.GetCurrentConfiguration())
        with self.robot:
            arm_plan = self.arm_planner.Plan(start_config, grasp_config)
            arm_plan = self.arm_planner.planning_env.ShortenPath(arm_plan)
        arm_traj = self.arm_planner.planning_env.herb.ConvertPlanToTrajectory(arm_plan)

        print 'Executing arm trajectory'
        self.arm_planner.planning_env.herb.ExecuteTrajectory(arm_traj)

        # Grasp the bottle
        task_manipulation = openravepy.interfaces.TaskManipulation(self.robot)
        task_manipulation.CloseFingers()

        raw_input('Press any key to exit...')

